<?php
if(!defined("FILE_ACCESS"))
    die("Access Granted! Donkey...");

class GetAdmin extends Settings
{   
    public $admin_id, $first_name, $last_name, $email, $role;

    public function __construct($id = null, $ignore_redirect = false)
    {
        parent::__construct();

        if(empty($id) || $id == null)
        {
            die("System Aborted... " . __CLASS__ . " : " . __LINE__);
        }

        $this -> GetConnection();

        $result = $this -> db -> query("select * from admin where id= '{$id}' limit 1 ");
        $row = $result -> fetch_assoc();

        $this -> first_name = trim(ucwords($row['first_name']));
        $this -> last_name = trim(ucwords($row['last_name']));
        $this -> email = trim(strtolower($row['email']));
        $this -> role = trim($row['role']);
    }
}