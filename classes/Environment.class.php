<?php

if(!defined("FILE_ACCESS"))
    die("Access Granted! Donkey...");

class Environment extends Settings
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Redirect($url, $redirect_method = "server")
    {
        if($redirect_method == "server")
        {
            if(!headers_sent())
            {
                exit(header("location: {$url} "));
            }
            else
            {
                ?><script>window.location="<?php echo $url; ?>";</script><?php
            }
        }
        else
        {
            ?><script>window.location="<?php echo $url; ?>";</script><?php
        }
    }

    public function JavascriptAlert($msg)
    {
        ?><script>window.alert("<?php echo $msg; ?>")</script><?php
    }

    public function AuthInit($auth_param = array("admin_id"), $after_auth_url = "", $auth_method = "")
    {
        foreach($auth_param as $index => $auth_name)
        {
            if(empty($auth_method) || $auth_method == "session")
            {
                if(!session_id()) { die("Calling '" . __FUNCTION__ . "' function, when no session_start() was invoked."); }
                if(!isset($_SESSION[$auth_name]))
                {
                    if(empty($after_auth_url)) // redirect to home then
                    {
                        $this -> Redirect($this -> Host);
                        exit;
                    }
                    else
                    {
                        $this -> Redirect($after_auth_url);
                        exit;
                    }
                }
            }
        }
    }

    public function EncodePassword($password)
    {
        return md5($password); // brb
    }

    public function LoadDropdown($current, $param_options)
    {
        foreach($param_options as $word => $value)
        {
            if($value == $current)
            {
                echo "<option value='{$value}' selected>{$word}</option>";
            }
            else if($value != $current)
            {
                echo "<option value='{$value}'>{$word}</option>";
            }
        }
    }

    public function TimeAgo($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
    
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
    
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
    
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

   public function RandomClass($classes = array())
   {
       
   } 
}