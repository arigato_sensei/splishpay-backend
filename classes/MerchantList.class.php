<?php

if(!defined("FILE_ACCESS"))
    die("Access Granted! Donkey.");

class MerchantList extends Settings
{
    public function __construct()
    {
        parent::__construct();
    }

    public function LoadList()
    {
        $this -> GetConnection();
        $environment = new Environment();

        $result = $this -> db -> query("select merchant.user_id, merchant.first_name, merchant.last_name, business.reg_name from 
        merchant left join business on merchant.user_id=business.user_id order by merchant.date_created desc ");

        $class_row = 3;

        while($row = $result -> fetch_assoc())
        {
            if($class_row > 3) { $class_row = 0; } else { $class_row += 1; }

            if($class_row >= 3) { echo "<!-- begin_row --> <div class='row'>"; }

            echo "<div class='col-xl-3 col-sm-6'>
            <div class='card'>
                <div class='card-body text-center'>
                    <div class='card-img-actions d-inline-block mb-3'>
                        <img class='img-fluid rounded-circle' src='assets/global_assets/images/placeholders/placeholder.jpg' width='170' height='170' alt=''>
                       
                    </div>

                    <h6 class='font-weight-semibold mb-0'>{$row['first_name']} {$row['last_name']}</h6>
                    <span class='d-block text-muted'>{$row['reg_name']}</span>

                    <div class='list-icons list-icons-extended mt-3'>
                        <a href='{$environment -> Href['admin']['merchant']}?mode=read&id={$row['user_id']}'
                        class='btn btn-danger rounded-round'><i class='icon-help mr-2'></i> More</a>
                    </div>
                </div>
            </div>
        </div>";

        if($class_row == 3) { echo "<!-- end_row --></div>"; }

        }
    }
}