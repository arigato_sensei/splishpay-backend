<?php
session_start();
if(!defined("FILE_ACCESS"))
    die("Access Granted! Donkey.");

class Dashboard extends Settings
{
    protected $error_response = "No Error";
    protected $success_response = 0;
    private $environment;
    
    public function __construct()
    {
        parent::__construct();
        $this -> environment = new Environment();
    }
    
    public function __call($method, $args)
    {
        $this -> environment -> $method($args[0]);
    }

    public function DashboardAdmin()
    {
        date_default_timezone_set('UTC');
        $searchq = "";
        $curl = curl_init();

        $_SESSION["m_users"] = NULL;

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://splishpayv1.herokuapp.com/api/v1/admin/get-users',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
              'Authorization: '.$_SESSION["token"].'',
              'Cookie: sails.sid=s%3A6eL2wk3C3Q3GmVgsmVBrTqD_E6k0iWce.R%2FGq2lSx266tKWfYMkV69a5c2CIkXHfwIjQ8Cwdjj04'
            ),
          ));

        $response = curl_exec($curl);

        if ($e = curl_error($curl)) {
            echo $e;
        }
        else
        {
            $res = json_decode($response, true);

            $_SESSION["mt_users"] = $res["count"]; // total number of users
            $_SESSION["m_users"] = $res["users"]; // users data

            $mUsers = $res["users"];
            $mNewUsers = array();

            foreach($mUsers as $mUser) { 
                if((time() - $mUser["createdAt"]) < 1 || $mUser["isVerfied"] == "false" ) {
                   array_push($mNewUsers, $mUser);
               };
            }

            $_SESSION["new_users"] = count($mNewUsers);
           
        }

        curl_close($curl);


        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://splishpayv1.herokuapp.com/api/v1/admin/get-support-tickets',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NDUsInJvbGUiOiJTVVBFUiBBRE1JTiIsImlhdCI6MTYyOTQ4MjQ5MX0.0Fzd_qtvdAiDCIlKw3X0fesvFITMrODDQLKv2IFbLIE',
            'Cookie: sails.sid=s%3A87IbICPJU_m1ERdzLsNfb_2rVEHfvLi_.1fVjoFrrKnfm6ZBBbhoVUb6U78n6pkMMlS6flAQKth4'
        ),
        ));

        $response = curl_exec($curl);

        if ($e = curl_error($curl)) {
            echo $e;
        }
        else
        {
            $res = json_decode($response, true);

            $_SESSION["tickets"] = $res["tickets"];
           
        }

        curl_close($curl);


        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://splishpayv1.herokuapp.com/api/v1/admin/get-transactions',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: ',
            'Cookie: sails.sid=s%3AcmtLVwe2gEfIl3qsCtGE_MM9mqDbNFN0.IhBPyfsqXCrJ2vSWRyPcQQiUffbBgecyCb%2F8hXoeFtc'
        ),
        ));

        $response = curl_exec($curl);

        if ($e = curl_error($curl)) {
            echo $e;
        }
        else
        {
            $res = json_decode($response, true);

            $mTransactions = $res["data"];

            $_SESSION["transactions"] = $mTransactions;
            $_SESSION["overall_no_of_transactions"] = count($res["data"]);
            $_SESSION["month_no_of_transactions"] = [];
            $_SESSION["T_income"] = 0;
            $_SESSION["day_transactions"] = [];
            $_SESSION["D_income"] = 0;

            foreach($_SESSION["transactions"] as $mTransaction)

            foreach($mTransactions as $mTransaction) {
                if (date('h', $mTransaction['createdAt']) >= 672){
                    array_push($_SESSION["month_no_of_transactions"], $mTransaction);
                }

                $_SESSION["T_income"] = $_SESSION["T_income"] + $mTransaction["amount"];

                if (date('h', $mTransaction['createdAt']) <= 24){
                    array_push($_SESSION["day_transactions"], $mTransaction);
                    $_SESSION["D_income"] = $_SESSION["D_income"] + $mTransaction["amount"];
                }
            }

            
           
        }

        curl_close($curl);

    }

   
    
}