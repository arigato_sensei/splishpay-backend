<?php

if(!defined("FILE_ACCESS"))
    die("Access Granted! Donkey...");

class Login extends Settings
{
    protected $error_response = "No Error";
    protected $success_response = 0;
    private $environment;
    
    public function __construct()
    {
        parent::__construct();
        $this -> environment = new Environment();
    }
    
    public function __call($method, $args)
    {
        $this -> environment -> $method($args[0]);
    }

    public function LoginAdmin()
    {
        $email = isset($_POST['email']) ? trim(strtolower($_POST['email'])) : "";
        $password = isset($_POST['password']) ? trim($_POST['password']) : "";

        if(empty($email))
        {
            echo $this -> PrintError("Email address is required");
        }
        else if(!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            echo $this -> PrintError("The specified email does not appear valid");
        }
        else if(empty($password))
        {
            echo $this -> PrintError("The password field is empty");
        }
        else
        {
            $this -> GetConnection();
            $environment = new Environment();
            
            // $password = $environment -> EncodePassword($password);

            $mURL = "https://splishpayv1.herokuapp.com/api/v1/admin/login";

            $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://splishpayv1.herokuapp.com/api/v1/admin/login',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "email": "'.$email.'",
                    "password":"'.$password.'"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Cookie: sails.sid=s%3Ak8CSAz3pFg8hDEd4pWXObv5ZUilThNWw.DRi2CFsvJBxJUcLi4AaB24EuojU9jyHdMyXH%2BabFyhI'
                ),
                ));

                $response = curl_exec($curl);
                

                if ($e = curl_error($curl)) {
                        echo $e;
                    }
                    else
                    {
                        $res = json_decode($response, true);
                        // echo($res["message"]);
        
                        $_SESSION["token"] = $res["token"];
                        $_SESSION["user"] = $res["user"];
                        $_SESSION["message"] = $res["message"];
        
                        // print_r($_SESSION["token"]);
        
                        $this -> UpdateForensic($res["user"]["id"]);
                            
                        $environment -> Redirect($this -> Href['admin']['dashboard']);
                       
                    }

                curl_close($curl);     

        }
    }

    private function UpdateForensic($admin_id)
    {
        $device = isset($_POST['device']) ? $_POST['device'] : "";
        $browser = isset($_POST['browser']) ? $_POST['browser'] : "";
        $os = isset($_POST['os']) ? $_POST['os'] : "";
        $ip = getenv("REMOTE_ADDR");

        $this -> GetConnection();

        // $stmt = $this -> db -> prepare("UPDATE admin set device= ?, os= ?, browser= ?, ip= ? WHERE id= ? limit 1 ");
        // $stmt -> bind_param('ss', $device, $os, $browser, $ip, $admin_id);
        // $stmt -> execute();
        // $stmt -> close(); // closed stream here
    }

    private function PrintError($msg)
    {
        return "<div class='alert alert-outline alert-danger' role='alert'>{$msg}</div>";
    }
}