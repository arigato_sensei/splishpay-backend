<?php

if(!defined("FILE_ACCESS"))
    die("Access Granted! Donkey.");

class Merchant extends Settings
{
    public function __construct()
    {
        parent::__construct();
    }

    public function LoadList($merchant_id)
    {
        $this -> GetConnection();

        $result = $this -> query("select * from merchant order by date_created desc ");

        $class_row = 3;

        while($row = $result -> fetch_assoc())
        {
            if($class_row == 3) { echo "<div class='row'>"; }

            echo "<div class='col-xl-3 col-sm-6'>
            <div class='card'>
                <div class='card-body text-center'>
                    <div class='card-img-actions d-inline-block mb-3'>
                        <img class='img-fluid rounded-circle' src='assets/global_assets/images/placeholders/placeholder.jpg' width='170' height='170' alt=''>
                        <div class='card-img-actions-overlay card-img rounded-circle'>
                            <a href='#' class='btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round'>
                                <i class='icon-plus3'></i>
                            </a>
                            <a href='user_pages_profile.html' class='btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2'>
                                <i class='icon-link'></i>
                            </a>
                        </div>
                    </div>

                    <h6 class='font-weight-semibold mb-0'>Merchant Name</h6>
                    <span class='d-block text-muted'>Business Name</span>

                    <div class='list-icons list-icons-extended mt-3'>
      <button type='button' class='btn btn-danger rounded-round'><i class='icon-help mr-2'></i> More</button>
                    </div>
                </div>
            </div>
        </div>";

        if($class_row == 3) { echo "</div>"; }
        $class_row +=1 ;

        }
    }
}