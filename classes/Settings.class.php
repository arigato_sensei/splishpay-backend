<?php

if(!defined("FILE_ACCESS"))
    die("Access Granted! Donkey...");

class Settings extends DBConnection
{
    public $App_Name = "SplishPay";
    public $Alt_App_Name = "Virtual Church";
    public $Short_App_Name = "V-Church";
    public $Logo_Url;
    public $Host = "";
    public $Support_Email = "support@splishpay.com";
    public $No_Reply_Email = "support@splishpay.com";
    public $Support_Phone = "";
    public $Href = array
    (
        "auth" => array
        (
            "login" => "login",
            "logout" => "logout"
        ),
        "admin" => array
        (
            "dashboard" => "dashboard",
            "merchant" => "merchant",
            "merchant-list" => "merchant-list"
        ),
        "merchant" => array
        (
            "merchant" => "merchant",
            "list" => "merchant-list"
        ),
        "customer" => array
        (
            "customer" => "customer",
            "list" => "customer-list"
        )
    );

    const PARTITION_DIR = "partition";
    const HEADER_DIR = "partition/header";
    const FOOTER_DIR = "partition/footer";
    const SERVER_LIB_DIR = "partition/library/server";

    public $SMTP = array
    (
        "HOST" => "email-smtp.us-east-1.amazonaws.com",
        "FROM" => "devops@celz5.org",
        "USERNAME" => "AKIAVVFLHUXMVHOIPS4P",
        "PASSWORD" => "BNZIKGvH+XAY5RbIP6kCGXLIHTbWiSZyD7qZKnUAwpx7",
        "PORT" => 587,
        "LAYER" => "tls",
        "DEBUG" => 0
    );

    public $title_list = array
    (
        "Pastor" => "Pst.",
        "Reverend" => "Rev.",
        "Deacon" => "Dcn.",
        "Deaconess" => "Dcns.",
        "Brother" => "Bro.",
        "Sister" => "Sis.",
        "Mr" => "Mr.",
        "Mrs" => "Mrs.",
        "Evangelist" => "Evangelist",
        "Bishop" => "Bishop",
        "Prophet" => "Prophet"
    );

    public $marital_status_list = array
    (
        "Single" => "Single",
        "Married" => "Married"
    );

    public $meeting_active_status = array
    (
        "ENABLE" => "1",
        "DISABLE" => "0"
    );

    public function __construct()
    {
        parent::__construct();
        
        $this -> GetLogo();
    }

    protected function GetLogo($type = "default")
    {
        switch($type)
        {
            case "default": $this -> Logo_Url = $this -> Host . "/assets/images/logo/default.png";
                break;
            case "light": $this -> Logo_Url = $this -> Host . "/assets/images/logo/light.png";
                break;
            default:
        }
    }

    public function PasswordHash($password)
    {
        return md5($password); // brb
    }
}