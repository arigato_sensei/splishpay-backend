<?php

class DbConnection
{
	protected $connection;
    public $db;
	private static $_instance; //The single instance
	const hostname_login = "localhost"; //192.168.64.2
    const database_login = "splitnzr_main";
	
	const username_login = "splitnzr_primary_user";
	const password_login = "Anonymous0198??";
	const live_mode = 0; // set to 1 once online
	/*
	Get an instance of the Database
	@return Instance
	*/
	public static function getInstance() 
                {
		if(!self::$_instance) 
                    { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	// Constructor
	public function __construct() 
        {
		 $this -> connection = new mysqli(self::hostname_login , self::username_login, self::password_login, self::database_login);
	
		// Error handling
		if(!$this -> connection) 
                    { 
					if(self::live_mode == 0)
					{
						trigger_error("Failed to connnect to MySQL: " . $this -> connection -> connect_error, E_USER_ERROR);
					}
		}
		else
		{
			$this -> db = $this -> connection;
		}
	}
        
	
	private function __clone() { }
	
	public function GetConnection() 
                {
		return $this -> db = $this -> connection;
	}
}
date_default_timezone_set('Africa/Lagos'); // Boss, kindly update this for Dynamic Global Time. [ Only if the project is serving other country ]