<?php
session_start();
define("FILE_ACCESS", TRUE);
require_once("classes/DBConnection.class.php");
require_once("classes/Settings.class.php");
require_once("classes/Environment.class.php");
require_once("classes/Login.class.php");

$environment = new Environment();
$environment -> Redirect($environment -> Href['auth']['login']); // Redirect back to Login

?>