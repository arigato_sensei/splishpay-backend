<?php

if(!defined("FILE_ACCESS"))
    die("Access Granted! Donkey...");
    
?>

<footer class="footer">
      <div>
        <span>&copy; <?php echo date("Y"); ?> <?php echo $environment -> App_Name; ?> v1.0.0. </span>
        <span>All Rights Reserved <a href="<?php echo $environment -> Host; ?>"><?php echo $environment -> App_Name; ?></a></span>
      </div>
      <div>
        <nav class="nav">
          <a href="#" class="nav-link">Licenses</a>
          <a href="#" class="nav-link">Change Log</a>
          <a href="#" class="nav-link">Get Help</a>
        </nav>
      </div>
    </footer>