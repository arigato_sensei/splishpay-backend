<?php
session_start();
define("FILE_ACCESS", TRUE);
require_once("classes/DBConnection.class.php");
require_once("classes/Settings.class.php");
require_once("classes/Environment.class.php");
require_once("classes/Login.class.php");

$environment = new Environment();

if(isset($_SESSION['admin_id']))
{
	$environment -> Redirect($environment -> Href['admin']['dashboard']); // Redirect to the Administrative Panel
}

require_once("auth/partition/library/server/which-browser/vendor/autoload.php");
$detect = new WhichBrowser\Parser(getallheaders());

$login = new Login();

?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="auth/assets/img/favicon.png">

    <title><?php echo $environment -> App_Name; ?> - Secured Management Panel</title>

    <!-- vendor css -->
    <link href="auth/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="auth/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="auth/assets/css/dashforge.css">
    <link rel="stylesheet" href="auth/assets/css/dashforge.auth.css">
  </head>
  <body>

   

    <div class="content content-fixed content-auth">
      <div class="container">
        <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
          <div class="media-body align-items-center d-none d-lg-flex">
            <div class="mx-wd-600">
              <img src="auth/assets/img/splish_login.png" class="img-fluid" alt="">
            </div>
            <hr/>
            <div class="pos-absolute b-0 l-0 tx-12 tx-center">
              <?php echo $environment -> App_Name; ?> Secured Panel 
              <a href="<?php echo $environment -> Host; ?>" target="_blank"> by Oweh Oreva</a>
            </div>
          </div><!-- media-body -->
          <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60">
            <div class="wd-100p">
              <h3 class="tx-color-01 mg-b-5">Sign In</h3>
              
              <?php
              if(isset($_POST['signin']))
              {
                  $login -> LoginAdmin();
              }
              else
              {
                  echo "<p class='tx-color-03 tx-16 mg-b-40'>Welcome back! Please signin to continue.</p>";
              }
              ?>

              <form action="" method="post">

              <div class="form-group">
                <label>Email address</label>
                <input type="email" name="email" class="form-control" placeholder="yourname@yourmail.com"
                value="<?php echo isset($_POST['email']) ? trim(strtolower($_POST['email'])) : ""; ?>"
                >
              </div>
              <div class="form-group">
                <div class="d-flex justify-content-between mg-b-5">
                  <label class="mg-b-0-f">Password</label>
                  <a href="" class="tx-13">Forgot password?</a>
                </div>
                <input type="password" name="password" class="form-control" placeholder="Enter your password">
              </div>
              <button class="btn btn-brand-02 btn-block" name="signin">Sign In</button>

              <input type="hidden" name="device" value="<?php echo $detect -> device -> type; ?>"/>
              <input type="hidden" name="os" value="<?php echo $detect -> os -> name; ?>"/>
              <input type="hidden" name="browser" value="<?php echo $detect -> browser -> name; ?>"/>

              </form>

              <div class="divider-text">or</div>
              <button class="btn btn-outline-facebook btn-block">Sign In With Facebook</button>
              <button class="btn btn-outline-twitter btn-block">Sign In With Twitter</button>
              <div class="tx-13 mg-t-20 tx-center">Don't have an account? <a href="#">Create an Account</a></div>
            </div>
          </div><!-- sign-wrapper -->
        </div><!-- media -->
      </div><!-- container -->
    </div><!-- content -->

    <footer class="footer">
      <div>
        <span>&copy; <?php echo date("Y"); ?> <?php echo $environment -> App_Name; ?> v1.0.0. </span>
        <span>All Rights Reserved <a href="<?php echo $environment -> Host; ?>"><?php echo $environment -> App_Name; ?></a></span>
      </div>
      <div>
        <nav class="nav">
          <a href="#" class="nav-link">Licenses</a>
          <a href="#" class="nav-link">Change Log</a>
          <a href="#" class="nav-link">Get Help</a>
        </nav>
      </div>
    </footer>

    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/feather-icons/feather.min.js"></script>
    <script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <script src="assets/js/dashforge.js"></script>

    <!-- append theme customizer -->
    <script src="lib/js-cookie/js.cookie.js"></script>
    <script src="assets/js/dashforge.settings.js"></script>
  </body>
</html>
