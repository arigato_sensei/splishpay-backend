<?php
session_start();
define("FILE_ACCESS", TRUE);
require_once("classes/DBConnection.class.php");
require_once("classes/Settings.class.php");
require_once("classes/Environment.class.php");
require_once("classes/GetAdmin.class.php");
require_once("classes/MerchantList.class.php");

$environment = new Environment();
$environment -> AuthInit();

$get_admin = new GetAdmin(isset($_SESSION['admin_id']) ? $_SESSION['admin_id'] : "");

$merchant_list = new MerchantList();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Merchant List - <?php echo $environment -> App_Name; ?></title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="assets/global_assets/js/main/jquery.min.js"></script>
	<script src="assets/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="assets/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="assets/global_assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="assets/js/app.js"></script>
	<!-- /theme JS files -->

</head>

<body>

<?php require_once("partition/header/header.authenticated.php"); ?>

<!-- Page header -->
<div class="page-header">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> Up one Level</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<!-- Page content -->
	<div class="page-content pt-0">


		<!-- Main content -->
		<div class="content-wrapper">


			<!-- Content area -->
			<div class="content">

				<!-- Square thumbs -->
				<div class="mb-3">
					<h6 class="mb-0 font-weight-semibold">
						Square thumbs
					</h6>
					<span class="text-muted d-block">Basic style using <code>card</code> component</span>
				</div>

				<?php $merchant_list -> LoadList(); ?>

			</div>
			<!-- /content area -->


		<?php require_once("partition/footer/footer.authenticated.php"); ?>

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>