<?php

define("FILE_ACCESS", TRUE);
require_once("classes/DBConnection.class.php");
require_once("classes/Settings.class.php");
require_once("classes/Environment.class.php");
require_once("classes/GetAdmin.class.php");
require_once("classes/Dashboard.class.php");

$environment = new Environment();

$dashboard = new Dashboard();

if($_SESSION["token"] === NULL)
    {
		echo "<p class='tx-color-03 tx-16 mg-b-40'>Sorry session is over.</p>";
    }
    else
    {
    	$dashboard -> DashboardAdmin();
		$get_admin = new GetAdmin(isset($_SESSION["user"]["id"]) ? $_SESSION["user"]["id"] : "");
    }

// $environment -> AuthInit();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Dashboard - <?php echo $environment -> App_Name; ?></title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="assets/global_assets/js/main/jquery.min.js"></script>
	<script src="assets/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="assets/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="assets/global_assets/js/plugins/ui/slinky.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="assets/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="assets/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script src="assets/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="assets/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script src="assets/global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="assets/global_assets/js/plugins/pickers/daterangepicker.js"></script>

	<script src="assets/js/app.js"></script>
	<script src="assets/global_assets/js/demo_pages/dashboard.js"></script>
	<!-- /theme JS files -->

</head>

<body>

<?php require_once("partition/header/header.authenticated.php"); ?>

	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>

			<div class="header-elements d-none py-0 mb-3 mb-md-0">
				<div class="breadcrumb">
					<a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">Dashboard</span>
				</div>
			</div>
		</div>
	</div>
	<!-- /page header -->
		

	<!-- Page content -->
	<div class="page-content pt-0">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content">

				<!-- Main charts -->
				<div class="row">
					<div class="col-xl-8">

						<!-- Users -->
						<div class="card">
							<div class="card-header header-elements-inline">
								<h6 class="card-title">Users</h6>
								<div class="header-elements">
									<div class="form-check form-check-right form-check-switchery form-check-switchery-sm">
										<label class="form-check-label">
											Live update:
											<input type="checkbox" class="form-input-switchery" checked data-fouc>
										</label>
									</div>
								</div>
							</div>

							<div class="card-body py-0">
								<div class="row">
									<div class="col-sm-4">
										<div class="d-flex align-items-center justify-content-center mb-2">
											<a href="#" class="btn bg-transparent border-teal text-teal rounded-round border-2 btn-icon mr-3">
												<i class="icon-plus3"></i>
											</a>
											<div>
												<div class="font-weight-semibold">No of Users</div>
												<span class="text-muted"><?php echo $_SESSION["mt_users"] ?></span>
											</div>
										</div>
										<div class="w-75 mx-auto mb-3" id="new-visitors"></div>
									</div>

									<div class="col-sm-4">
										<div class="d-flex align-items-center justify-content-center mb-2">
											<a href="#" class="btn bg-transparent border-warning-400 text-warning-400 rounded-round border-2 btn-icon mr-3">
												<i class="icon-watch2"></i>
											</a>
											<div>
												<div class="font-weight-semibold">New Users</div>
												<span class="text-muted"><?php echo $_SESSION["new_users"] ?></span>
											</div>
										</div>
										<div class="w-75 mx-auto mb-3" id="new-sessions"></div>
									</div>

									<div class="col-sm-4">
										<div class="d-flex align-items-center justify-content-center mb-2">
											<a href="#" class="btn bg-transparent border-indigo-400 text-indigo-400 rounded-round border-2 btn-icon mr-3">
												<i class="icon-people"></i>
											</a>
											<div>
												<div class="font-weight-semibold">Total online</div>
												<span class="text-muted"><span class="badge badge-mark border-success mr-2"></span>0</span>
											</div>
										</div>
										<div class="w-75 mx-auto mb-3" id="total-online"></div>
									</div>
								</div>
							</div>

						</div>
						<!-- /traffic sources -->

					</div>

					<div class="col-xl-4">

						<!-- Sales stats -->
						<div class="card">
							<div class="card-header header-elements-inline">
								<h6 class="card-title">Transactions</h6>
								<div class="header-elements">
									<select class="form-control" id="select_date" data-fouc>
										<option value="val1">June, 29 - July, 5</option>
										<option value="val2">June, 22 - June 28</option>
										<option value="val3" selected>June, 15 - June, 21</option>
										<option value="val4">June, 8 - June, 14</option>
									</select>
			                	</div>
							</div>

							<div class="card-body py-0">
								<div class="row text-center">
									<div class="col-4">
										<div class="mb-3">
											<h5 class="font-weight-semibold mb-0"><?php echo count($_SESSION["transactions"]) ?></h5>
											<span class="text-muted font-size-sm">Overall</span>
										</div>
									</div>

									<div class="col-4">
										<div class="mb-3">
											<h5 class="font-weight-semibold mb-0"><?php echo count($_SESSION["month_no_of_transactions"]) ?></h5>
											<span class="text-muted font-size-sm">Month</span>
										</div>
									</div>

									<div class="col-4">
										<div class="mb-3">
											<h5 class="font-weight-semibold mb-0"><?php echo "₦ ".number_format($_SESSION["T_income"], 2); ?></h5>
											<span class="text-muted font-size-sm">Income</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /sales stats -->

					</div>
				</div>
				<!-- /main charts -->


				<!-- Dashboard content -->
				<div class="row">
					<div class="col-xl-8">

						<!--Users DashbTableoard-->
						<div class="card">
							<div class="card-header header-elements-sm-inline">
								<h6 class="card-title">Users Table</h6>
								<div class="header-elements">
									<span class="badge bg-success badge-pill"><?php echo $_SESSION["new_users"]?> new users</span>
									<div class="list-icons ml-3">
				                		<div class="list-icons-item dropdown">
				                			<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
											<div class="dropdown-menu">
												<a href="#" class="dropdown-item"><i class="icon-sync"></i> Update data</a>
												<a href="#" class="dropdown-item"><i class="icon-list-unordered"></i> Detailed log</a>
												<a href="#" class="dropdown-item"><i class="icon-pie5"></i> Statistics</a>
												<div class="dropdown-divider"></div>
												<a href="#" class="dropdown-item"><i class="icon-cross3"></i> Clear list</a>
											</div>
				                		</div>
				                	</div>
			                	</div>
							</div>

							<div class="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap">
								<div class="d-flex">
								<form class="input-group" style="width:700px" action="dashboard.php" method="post">
									
									<input type="search" id="search" name="search" class="form-control" />
									<!-- <label class="form-label" for="form1">Search</label> -->
									
									<button type="submit" class="btn btn-primary">
										<i class="icon-search4"></i>
									</button>
								</form>
								</div>

								<div>
									<a href="#" class="btn bg-indigo-300"><i class="icon-statistics mr-2"></i> Show All</a>
								</div>
							</div>

							<div class="table-responsive">
								<table class="table text-nowrap">
									<thead>
										<tr>
											<th>Name</th>
											<th>Email</th>
											<th>Last Updated</th>
											<th>Transactions</th>
											<th>Status</th>
											<th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
										</tr>
									</thead>
									<tbody>
										<?php
											if(isset($_POST["search"])) {
												$searchq = $_POST["search"];
												// $searchq = preg_replace("#[^0-9a-z]#i","",$searchq);
												$mUsers = $_SESSION["m_users"];
												//TODO: User Search Engine
												foreach($mUsers as $mUser) {
													if($searchq == $mUser["email"]){
														?>

											<tr>
											<td>
												<div class="d-flex align-items-center">
												<div class="mr-3">
														<a href="#" class="btn bg-primary-400 rounded-round btn-icon btn-sm">
															<span class="letter-icon"></span>
														</a>
													</div>
													<div>
														<a href="#" class="text-default font-weight-semibold"><?php echo $mUser["name"] ?></a>
														<div class="text-muted font-size-sm">
															<span class="badge badge-mark border-blue mr-1"></span>
															<?php echo date('m/d/y', substr($mUser["createdAt"], 0, 10)) ?>
														</div>
													</div>
												</div>
											</td>
											<td><span class="text-muted"><?php echo $mUser["email"] ?></span></td>
											<td><span class="text-success-600"><i class="icon-stats-growth2 mr-2"></i> <?php echo date('m/d/y', $mUser["updatedAt"]) ?></span></td>
											<td><h6 class="font-weight-semibold mb-0">₦
												<?php
												$mTrans = 0;
												foreach($_SESSION["transactions"] as $mTransaction) {
													if($mTransaction["owner"]["id"] == $mUser["id"])
													{
														$mTrans = $mTrans + $mTransaction["amount"];
														
													}
												}
												echo $mTrans;
											?></h6></td>
											<td><?php
											
											if($mUser["isVerfied"] == FALSE)
											{ ?>
											 <span class="badge bg-danger"><?php echo("False"); ?></span>
											<?php } else
											{
											?>
											<span class="badge bg-blue"><?php echo("True"); ?></span>
											<?php }
											
											?></td>
											<td class="text-center">
												<div class="list-icons">
													<div class="list-icons-item dropdown">
														<a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
														<div class="dropdown-menu dropdown-menu-right">
															<a href="#" class="dropdown-item"><i class="icon-file-stats"></i> View statement</a>
															<a href="#" class="dropdown-item"><i class="icon-file-text2"></i> Edit campaign</a>
															<a href="#" class="dropdown-item"><i class="icon-file-locked"></i> Disable campaign</a>
															<div class="dropdown-divider"></div>
															<a href="#" class="dropdown-item"><i class="icon-gear"></i> Settings</a>
														</div>
													</div>
												</div>
											</td>
										</tr><?php
													}  
												}
											}
											else
											{
											$row = 1;
											foreach($_SESSION["m_users"] as $mUser) {  
												if($row <= 5) {												
												?>
												<tr>
											<td>
												<div class="d-flex align-items-center">
												<div class="mr-3">
														<a href="#" class="btn bg-primary-400 rounded-round btn-icon btn-sm">
															<span class="letter-icon"></span>
														</a>
													</div>
													<div>
														<a href="#" class="text-default font-weight-semibold"><?php echo $mUser["name"] ?></a>
														<div class="text-muted font-size-sm">
															<span class="badge badge-mark border-blue mr-1"></span>
															<?php echo date('m/d/y', $mUser["createdAt"]) ?>
														</div>
													</div>
												</div>
											</td>
											<td><span class="text-muted"><?php echo $mUser["email"] ?></span></td>
											<td><span class="text-success-600"><i class="icon-stats-growth2 mr-2"></i> <?php echo date('m/d/y', substr($mUser["createdAt"], 0, 10)) ?></span></td>
											<td><h6 class="font-weight-semibold mb-0">
												<?php
												$mTrans = 0;
												foreach($_SESSION["transactions"] as $mTransaction) {
													if($mTransaction["owner"]["id"] == $mUser["id"])
													{
														$mTrans = $mTrans + $mTransaction["amount"];
													}
												}
												
												
												echo "₦ ".number_format($mTrans, 2);
											?>
											</h6></td>
											<td><?php
											
											if($mUser["isVerfied"] == FALSE)
											{ ?>
											 <span class="badge bg-danger"><?php echo("False"); ?></span>
											<?php } else
											{
											?>
											<span class="badge bg-blue"><?php echo("True"); ?></span>
											<?php }
											
											?></td>
											<td class="text-center">
												<div class="list-icons">
													<div class="list-icons-item dropdown">
														<a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
														<div class="dropdown-menu dropdown-menu-right">
															<a href="#" class="dropdown-item"><i class="icon-file-stats"></i> View statement</a>
															<a href="#" class="dropdown-item"><i class="icon-file-text2"></i> Edit campaign</a>
															<a href="#" class="dropdown-item"><i class="icon-file-locked"></i> Disable campaign</a>
															<div class="dropdown-divider"></div>
															<a href="#" class="dropdown-item"><i class="icon-gear"></i> Settings</a>
														</div>
													</div>
												</div>
											</td>
										</tr>
										<?php
										$row = $row + 1;	
										}} }
										?>
										
									</tbody>
								</table>
							</div>
						</div>
						<!-- /marketing campaigns -->

						
						<!-- Support tickets -->
						<div class="card">
							<div class="card-header header-elements-sm-inline">
								<h6 class="card-title">Support tickets</h6>
								<div class="header-elements">
									<a class="text-default daterange-ranges font-weight-semibold cursor-pointer dropdown-toggle">
										<i class="icon-calendar3 mr-2"></i>
										<span></span>
									</a>
			                	</div>
							</div>

							<div class="card-body d-md-flex align-items-md-center justify-content-md-between flex-md-wrap">
								<div class="d-flex align-items-center mb-3 mb-md-0">
									<div id="tickets-status"></div>
									<div class="ml-3">
										<h5 class="font-weight-semibold mb-0">14,327 <span class="text-success font-size-sm font-weight-normal"><i class="icon-arrow-up12"></i> (+2.9%)</span></h5>
										<span class="badge badge-mark border-success mr-1"></span> <span class="text-muted">Jun 16, 10:00 am</span>
									</div>
								</div>

								<div class="d-flex align-items-center mb-3 mb-md-0">
									<a href="#" class="btn bg-transparent border-indigo-400 text-indigo-400 rounded-round border-2 btn-icon">
										<i class="icon-alarm-add"></i>
									</a>
									<div class="ml-3">
										<h5 class="font-weight-semibold mb-0">1,132</h5>
										<span class="text-muted">total tickets</span>
									</div>
								</div>

								<div class="d-flex align-items-center mb-3 mb-md-0">
									<a href="#" class="btn bg-transparent border-indigo-400 text-indigo-400 rounded-round border-2 btn-icon">
										<i class="icon-spinner11"></i>
									</a>
									<div class="ml-3">
										<h5 class="font-weight-semibold mb-0">06:25:00</h5>
										<span class="text-muted">response time</span>
									</div>
								</div>

								<div>
									<a href="#" class="btn bg-teal-400"><i class="icon-statistics mr-2"></i> Report</a>
								</div>
							</div>

							<div class="table-responsive">
								<table class="table text-nowrap">
									<thead>
										<tr>
											<th style="width: 50px">Due</th>
											<th style="width: 300px;">User</th>
											<th>Description</th>
											<th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
										</tr>
									</thead>
									<tbody>
									<tr class="table-active table-border-double">
											<td colspan="3">Active tickets</td>	
											<td class="text-right">
												<span class="badge bg-blue badge-pill">24</span>
											</td>
										</tr>

									<?php 
										$tickets = $_SESSION["tickets"];
										foreach($tickets as $ticket) {

											?>
										<tr>
											<td class="text-center">
												<h6 class="mb-0">
												<?php 
													$current_time = time();
													$interval = $current_time - $ticket["updatedAt"];
													$fmt_interval = date('h', $interval);
													echo($fmt_interval);
												?>
												</h6>
												<div class="font-size-sm text-muted line-height-1">hours</div>
											</td>
											<td>
												<div class="d-flex align-items-center">
													<div class="mr-3">
														<a href="#" class="btn bg-teal-400 rounded-round btn-icon btn-sm">
															<span class="letter-icon"></span>
														</a>
													</div>
													<div>
														<a href="#" class="text-default font-weight-semibold letter-icon-title">
															<?php echo $ticket["owner"]["name"]; ?>
														
														</a>
														<div class="text-muted font-size-sm"><span class="badge badge-mark border-blue mr-1"></span> Active</div>
													</div>
												</div>
											</td>
											<td>
												<a href="#" class="text-default">
													<div class="font-weight-semibold">[#<?php echo $ticket["id"] ?>] Workaround for OS X selects printing bug</div>
													<span class="text-muted"><?php echo (substr($ticket["text"], 0, 100)) ?></span>
												</a>
											</td>
											<td class="text-center">
												<div class="list-icons">
													<div class="list-icons-item dropdown">
														<a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
														<div class="dropdown-menu dropdown-menu-right">
															<a href="#" class="dropdown-item"><i class="icon-undo"></i> Quick reply</a>
															<a href="#" class="dropdown-item"><i class="icon-history"></i> Full history</a>
															<div class="dropdown-divider"></div>
															<a href="#" class="dropdown-item"><i class="icon-checkmark3 text-success"></i> Resolve issue</a>
															<a href="#" class="dropdown-item"><i class="icon-cross2 text-danger"></i> Close issue</a>
														</div>
													</div>
												</div>
											</td>
										</tr> <?php				
															
										}
									?>
										
										<tr class="table-active table-border-double">
											<td colspan="3">Closed tickets</td>
											<td class="text-right">
												<span class="badge bg-danger badge-pill">37</span>
											</td>
										</tr>

										<tr>
											<td class="text-center">
												<i class="icon-cross2 text-danger-400"></i>
											</td>
											<td>
												<div class="d-flex align-items-center">
													<div class="mr-3">
														<a href="#">
															<img src="assets/global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="32" height="32" alt="">
														</a>
													</div>
													<div>
														<a href="#" class="text-default font-weight-semibold">Mitchell Sitkin</a>
														<div class="text-muted font-size-sm"><span class="badge badge-mark border-danger mr-1"></span> Closed</div>
													</div>
												</div>
											</td>
											<td>
												<a href="#" class="text-default">
													<div>[#1040] Account for static form controls in form group</div>
													<span class="text-muted">Resizes control label's font-size and account for the standard...</span>
												</a>
											</td>
											<td class="text-center">
												<div class="list-icons">
													<div class="list-icons-item dropdown">
														<a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
														<div class="dropdown-menu dropdown-menu-right">
															<a href="#" class="dropdown-item"><i class="icon-undo"></i> Quick reply</a>
															<a href="#" class="dropdown-item"><i class="icon-history"></i> Full history</a>
															<div class="dropdown-divider"></div>
															<a href="#" class="dropdown-item"><i class="icon-plus3 text-blue"></i> Unresolve issue</a>
															<a href="#" class="dropdown-item"><i class="icon-spinner11 text-grey"></i> Reopen issue</a>
														</div>
													</div>
												</div>
											</td>
										</tr>

										<tr>
											<td class="text-center">
												<i class="icon-cross2 text-danger"></i>
											</td>
											<td>
												<div class="d-flex align-items-center">
													<div class="mr-3">
														<a href="#" class="btn bg-brown-400 rounded-round btn-icon btn-sm">
															<span class="letter-icon"></span>
														</a>
													</div>
													<div>
														<a href="#" class="text-default font-weight-semibold letter-icon-title">Katleen Jensen</a>
														<div class="text-muted font-size-sm"><span class="badge badge-mark border-danger mr-1"></span> Closed</div>
													</div>
												</div>
											</td>
											<td>
												<a href="#" class="text-default">
													<div>[#1038] Proper sizing of form control feedback</div>
													<span class="text-muted">Feedback icon sizing inside a larger/smaller form-group...</span>
												</a>
											</td>
											<td class="text-center">
												<div class="list-icons">
													<div class="list-icons-item dropdown">
														<a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
														<div class="dropdown-menu dropdown-menu-right">
															<a href="#" class="dropdown-item"><i class="icon-undo"></i> Quick reply</a>
															<a href="#" class="dropdown-item"><i class="icon-history"></i> Full history</a>
															<div class="dropdown-divider"></div>
															<a href="#" class="dropdown-item"><i class="icon-plus3 text-blue"></i> Unresolve issue</a>
															<a href="#" class="dropdown-item"><i class="icon-spinner11 text-grey"></i> Reopen issue</a>
														</div>
													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!-- /support tickets -->

					</div>

					<div class="col-xl-4">
						<!-- Daily sales -->
						<div class="card">
							<div class="card-header header-elements-inline">
								<h6 class="card-title">Transactions History</h6>
								<div class="header-elements">
									<span class="font-weight-bold text-danger-600 ml-2"><?php echo "₦ ".number_format($_SESSION["D_income"], 2); ?></span>
									<div class="list-icons ml-3">
				                		<div class="list-icons-item dropdown">
				                			<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i></a>
											<div class="dropdown-menu dropdown-menu-right">
												<a href="#" class="dropdown-item"><i class="icon-sync"></i> Update data</a>
												<a href="#" class="dropdown-item"><i class="icon-list-unordered"></i> Detailed log</a>
												<a href="#" class="dropdown-item"><i class="icon-pie5"></i> Statistics</a>
												<div class="dropdown-divider"></div>
												<a href="#" class="dropdown-item"><i class="icon-cross3"></i> Clear list</a>
											</div>
				                		</div>
				                	</div>
								</div>
							</div>

							<div class="card-body">
								<div class="chart" id="sales-heatmap"></div>
							</div>

							<div class="table-responsive">
								<table class="table text-nowrap">
									<thead>
										<tr>
											<th class="w-100">User</th>
											<th>Date</th>
											<th>Time</th>
											<th>Price</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										foreach($_SESSION["day_transactions"] as $mTrans)
										{ ?>
												<tr>
											<td>
												<div class="d-flex align-items-center">
													<div class="mr-3">
														<a href="#" class="btn bg-primary-400 rounded-round btn-icon btn-sm">
															<span class="letter-icon"></span>
														</a>
													</div>
													<div>
														<a href="#" class="text-default font-weight-semibold letter-icon-title"><?php echo $mTrans["owner"]["name"] ?></a>
														<div class="text-muted font-size-sm"><i class="icon-checkmark3 font-size-sm mr-1"></i> New order</div>
													</div>
												</div>
											</td>
											<td>
												<span class="text-muted font-size-sm"><?php echo date('d/m/y', substr($mTrans["updatedAt"], 0, 10)) ?></span>
											</td>
											<td>
												<span class="text-muted font-size-sm"><?php echo date('h:i a', $mTrans["updatedAt"]) ?></span>
											</td>
											<td>
												<h6 class="font-weight-semibold mb-0"><?php echo "₦ ".number_format($mTrans["amount"], 2); ?></h6>
											</td>
										</tr>

										<?php } ?>
										
									</tbody>
								</table>
							</div>
						</div>
						<!-- /daily sales -->

					</div>
				</div>
				<!-- /dashboard content -->

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->


	<?php require_once("partition/footer/footer.authenticated.php"); ?>
		
</body>
</html>
