<?php
session_start(); // initialize
session_destroy(); // clear

define("FILE_ACCESS", TRUE);
require_once("classes/DBConnection.class.php");
require_once("classes/Settings.class.php");
require_once("classes/Environment.class.php");

$environment = new Environment();

if(isset($_GET['return_url']))
{
    $environment -> Redirect($_GET['return_url']);
}
else
{
    $environment -> Redirect($environment -> Href['auth']['login']);
}