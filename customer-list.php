
<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

    <title>DashForge Responsive Bootstrap 4 Dashboard Template</title>

    <!-- vendor css -->
    <link href="lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="assets/css/dashforge.css">
    <link rel="stylesheet" href="assets/css/dashforge.profile.css">
  </head>
  <body class="page-profile">

    <?php require_once("partition/header/header.authenticated.php"); ?>

    <div class="content content-fixed bd-b">
      <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
        <div class="d-sm-flex align-items-center justify-content-between">
          <div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                <li class="breadcrumb-item"><a href="#">Your Profile</a></li>
                <li class="breadcrumb-item active" aria-current="page">Connections</li>
              </ol>
            </nav>
            <h4 class="mg-b-0">Connections</h4>
          </div>
          <div class="search-form mg-t-20 mg-sm-t-0">
            <input type="search" class="form-control" placeholder="Search people">
            <button class="btn" type="button"><i data-feather="search"></i></button>
          </div>
        </div>
      </div><!-- container -->
    </div><!-- content -->

    <div class="content">
      <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
        <div class="row">
          <div class="col-lg-9">
            <ul class="nav nav-line nav-line-profile mg-b-30">
              <li class="nav-item">
                <a href="" class="nav-link d-flex align-items-center active">Followers <span class="badge">340</span></a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link">Following <span class="badge">1,563</span></a>
              </li>
              <li class="nav-item d-none d-sm-block">
                <a href="" class="nav-link">Request  <span class="badge">19</span></a>
              </li>
            </ul>

            <div class="row row-xs mg-b-25">
              <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3">
                <div class="card card-profile">
                  <img src="https://via.placeholder.com/500" class="card-img-top" alt="">
                  <div class="card-body tx-13">
                    <div>
                      <a href="">
                        <div class="avatar avatar-lg"><img src="https://via.placeholder.com/350" class="rounded-circle" alt=""></div>
                      </a>
                      <h5><a href="">Zhen Juan Chiu</a></h5>
                      <p>Software Engineer</p>
                      <button class="btn btn-block btn-white">Follow</button>
                    </div>
                  </div>
                </div><!-- card -->
              </div><!-- col -->
              <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-t-10 mg-sm-t-0">
                <div class="card card-profile">
                  <img src="https://via.placeholder.com/500x281" class="card-img-top" alt="">
                  <div class="card-body tx-13">
                    <div>
                      <a href="">
                        <div class="avatar avatar-lg"><img src="https://via.placeholder.com/600" class="rounded-circle" alt=""></div>
                      </a>
                      <h5><a href="">Barbara Marion</a></h5>
                      <p>Tech Executive</p>
                      <button class="btn btn-block btn-white">Follow</button>
                    </div>
                  </div>
                </div><!-- card -->
              </div><!-- col -->
              <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-t-10 mg-sm-t-0">
                <div class="card card-profile">
                  <img src="https://via.placeholder.com/500" class="card-img-top" alt="">
                  <div class="card-body tx-13">
                    <div>
                      <a href="">
                        <div class="avatar avatar-lg"><span class="avatar-initial rounded-circle bg-teal">c</span></div>
                      </a>
                      <h5><a href="">Christine Arnold</a></h5>
                      <p>Lead Creative Design</p>
                      <button class="btn btn-block btn-primary">Unfollow</button>
                    </div>
                  </div>
                </div><!-- card -->
              </div><!-- col -->
              <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-t-10 mg-md-t-0 mg-lg-t-10 mg-xl-t-0">
                <div class="card card-profile">
                  <img src="https://via.placeholder.com/1000x666" class="card-img-top" alt="">
                  <div class="card-body tx-13">
                    <div>
                      <a href="">
                        <div class="avatar avatar-lg"><span class="avatar-initial rounded-circle bg-pink">n</span></div>
                      </a>
                      <h5><a href="">Natalie Corwin</a></h5>
                      <p>Product Designer</p>
                      <button class="btn btn-block btn-white">Follow</button>
                    </div>
                  </div>
                </div><!-- card -->
              </div><!-- col -->
              <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-t-10">
                <div class="card card-profile">
                  <img src="https://via.placeholder.com/1000x666" class="card-img-top" alt="">
                  <div class="card-body tx-13">
                    <div>
                      <a href="">
                        <div class="avatar avatar-lg"><span class="avatar-initial rounded-circle bg-gray-300">c</span></div>
                      </a>
                      <h5><a href="">Carolyn Park</a></h5>
                      <p>Lead Creative Design</p>
                      <button class="btn btn-block btn-white">Follow</button>
                    </div>
                  </div>
                </div><!-- card -->
              </div><!-- col -->
              <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-t-10">
                <div class="card card-profile">
                  <img src="https://via.placeholder.com/640x427" class="card-img-top" alt="">
                  <div class="card-body tx-13">
                    <div>
                      <a href="">
                        <div class="avatar avatar-lg"><span class="avatar-initial rounded-circle bg-gray-900">d</span></div>
                      </a>
                      <h5><a href="">Debbie Hite</a></h5>
                      <p>Lead Animator</p>
                      <button class="btn btn-block btn-primary">Unfollow</button>
                    </div>
                  </div><!-- card-body -->
                </div><!-- card -->
              </div><!-- col -->
              <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-t-10">
                <div class="card card-profile">
                  <img src="https://via.placeholder.com/640x360" class="card-img-top" alt="">
                  <div class="card-body tx-13">
                    <div>
                      <a href="">
                        <div class="avatar avatar-lg"><span class="avatar-initial rounded-circle bg-success">s</span></div>
                      </a>
                      <h5><a href="">Sandra Valles</a></h5>
                      <p>Software Architect</p>
                      <button class="btn btn-block btn-white">Follow</button>
                    </div>
                  </div><!-- card-body -->
                </div><!-- card -->
              </div><!-- col -->
              <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-t-10">
                <div class="card card-profile">
                  <img src="https://via.placeholder.com/640x426" class="card-img-top" alt="">
                  <div class="card-body tx-13">
                    <div>
                      <a href="">
                        <div class="avatar avatar-lg"><span class="avatar-initial rounded-circle bg-indigo">s</span></div>
                      </a>
                      <h5><a href="">Patrick Miramontes</a></h5>
                      <p>Software Engineer</p>
                      <button class="btn btn-block btn-white">Follow</button>
                    </div>
                  </div><!-- card-body -->
                </div><!-- card -->
              </div><!-- col -->
              <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-t-10">
                <div class="card card-profile">
                  <img src="https://via.placeholder.com/640x360" class="card-img-top" alt="">
                  <div class="card-body tx-13">
                    <div>
                      <a href="">
                        <div class="avatar avatar-lg"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
                      </a>
                      <h5><a href="">Amalia Redfern</a></h5>
                      <p>Front-end Engineer</p>
                      <button class="btn btn-block btn-primary">Unfollow</button>
                    </div>
                  </div><!-- card-body -->
                </div><!-- card -->
              </div><!-- col -->
              <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-t-10">
                <div class="card card-profile">
                  <img src="https://via.placeholder.com/640x427" class="card-img-top" alt="">
                  <div class="card-body tx-13">
                    <div>
                      <a href="">
                        <div class="avatar avatar-lg"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
                      </a>
                      <h5><a href="">Carole Rossignol</a></h5>
                      <p>Software Engineer</p>
                      <button class="btn btn-block btn-white">Follow</button>
                    </div>
                  </div><!-- card-body -->
                </div><!-- card -->
              </div><!-- col -->
            </div><!-- row -->

            <button class="btn btn-block btn-sm btn-white">Load more</button>
          </div><!-- col -->
          <div class="col-lg-3 mg-t-40 mg-lg-t-0">
            <div class="d-flex align-items-center justify-content-between mg-b-20">
              <h6 class="tx-uppercase tx-semibold mg-b-0">People You May Know</h6>
            </div>
            <ul class="list-unstyled media-list mg-b-15">
              <li class="media align-items-center">
                <a href=""><div class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div></a>
                <div class="media-body pd-l-15">
                  <h6 class="mg-b-2"><a href="" class="link-01">Allan Ray Palban</a></h6>
                  <span class="tx-13 tx-color-03">Senior Business Analyst</span>
                </div>
              </li>
              <li class="media align-items-center mg-t-15">
                <a href=""><div class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div></a>
                <div class="media-body pd-l-15">
                  <h6 class="mg-b-2"><a href="" class="link-01">Rhea Castanares</a></h6>
                  <span class="tx-13 tx-color-03">Product Designer</span>
                </div>
              </li>
              <li class="media align-items-center mg-t-15">
                <a href=""><div class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div></a>
                <div class="media-body pd-l-15">
                  <h6 class="mg-b-2"><a href="" class="link-01">Philip Cesar Galban</a></h6>
                  <span class="tx-13 tx-color-03">Executive Assistant</span>
                </div>
              </li>
              <li class="media align-items-center mg-t-15">
                <a href=""><div class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div></a>
                <div class="media-body pd-l-15">
                  <h6 class="mg-b-2"><a href="" class="link-01">Randy Macapala</a></h6>
                  <span class="tx-13 tx-color-03">Business Entrepreneur</span>
                </div>
              </li>
              <li class="media align-items-center mg-t-15">
                <a href=""><div class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div></a>
                <div class="media-body pd-l-15">
                  <h6 class="mg-b-2"><a href="" class="link-01">Abigail Johnson</a></h6>
                  <span class="d-block tx-13 tx-color-03">System Administrator</span>
                </div>
              </li>
            </ul>

            <h6 class="tx-uppercase tx-semibold mg-t-50 mg-b-15">Discover By Position</h6>

            <nav class="nav nav-classic tx-13">
              <a href="" class="nav-link"><span>Software Engineer</span> <span class="badge">20</span></a>
              <a href="" class="nav-link"><span>UI/UX Designer</span> <span class="badge">18</span></a>
              <a href="" class="nav-link"><span>Sales Representative</span> <span class="badge">14</span></a>
              <a href="" class="nav-link"><span>Product Representative</span> <span class="badge">12</span></a>
              <a href="" class="nav-link"><span>Full-Stack Developer</span> <span class="badge">10</span></a>
            </nav>

            <h6 class="tx-uppercase tx-semibold mg-t-50 mg-b-15">Discover By Location</h6>

            <nav class="nav nav-classic tx-13">
              <a href="" class="nav-link"><span>San Francisco, California</span> <span class="badge">20</span></a>
              <a href="" class="nav-link"><span>Los Angeles, California</span> <span class="badge">18</span></a>
              <a href="" class="nav-link"><span>Las Vegas, Nevada</span> <span class="badge">14</span></a>
              <a href="" class="nav-link"><span>Austin, Texas</span> <span class="badge">12</span></a>
              <a href="" class="nav-link"><span>Arlington, Nebraska</span> <span class="badge">10</span></a>
            </nav>
          </div><!-- col -->
        </div><!-- row -->
      </div><!-- container -->
    </div><!-- content -->

    <?php require_once("partition/footer/footer.authenticated.php"); ?>

    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/feather-icons/feather.min.js"></script>
    <script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <script src="assets/js/dashforge.js"></script>

    <!-- append theme customizer -->
    <script src="lib/js-cookie/js.cookie.js"></script>
    <script src="assets/js/dashforge.settings.js"></script>

  </body>
</html>
